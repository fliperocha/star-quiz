import React from 'react';
import { Button } from 'antd';
import { Link } from "react-router-dom";
import Logo from './../logo/Logo';

import src from '../../images/fourth.gif';

import './Home.css';

const Home = () => {
  return (
    <div className="Home">
      <div className="Home__content">
        <Logo width={600} />
        <div className="Home__content__container">
          <img src={src} style={{ width: '400px', height: '400px', borderRadius: '40px' }} />
          <div className="Home__content__description">
            Do you know the Star Wars characters? Huh? Then show us! 
            With Star Wars Quiz you will have the 
            opportunity to identify the main characters in Star Wars, score points and become a 
            powerful Jedi!
          </div>
          <div className="Home__content__button">
            <Link to="/game">
              <Button type="primary" size="large">
                PLAY!
              </Button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;