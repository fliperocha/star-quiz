import React from 'react';
import Logo from './../logo/Logo';

import './Ranking.css';

const Ranking = ({ nickname, score }) => {
  return (
    <div className="Ranking">
      <div className="Ranking__content">
        <Logo width={400} />
        <h2>{nickname}: {score}</h2>
        <h3>
          "I'm your father!"
        </h3>
        <h5>
          "Thank you Padawan!"
        </h5>
      </div>
    </div>
  );
};

export default Ranking;